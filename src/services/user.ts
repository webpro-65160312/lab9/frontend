import type { User } from '@/types/User'
import http from './http'

function addUser(user: User & { files: File[] }) {
  const form = new FormData()
  form.append('email', user.email)
  form.append('password', user.password)
  form.append('fullName', user.fullName)
  form.append('gender', user.gender)
  form.append('roles', JSON.stringify(user.roles))
  form.append('files', user.files[0])
  return http.post('/users', form, { headers: { 'Content-Type': 'multipart/form-data' } })
}

function updateUser(user: User & { files: File[] }) {
  const form = new FormData()
  form.append('email', user.email)
  form.append('password', user.password)
  form.append('fullName', user.fullName)
  form.append('gender', user.gender)
  form.append('roles', JSON.stringify(user.roles))
  if (user.files) {
    form.append('files', user.files[0])
  }
  return http.post(`/users/${user.id}`, form, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUser(id: number) {
  return http.get(`/users/${id}`)
}

function getUsers() {
  return http.get('/users')
}

export default { addUser, updateUser, delUser, getUser, getUsers }
