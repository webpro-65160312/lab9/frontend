import http from './http'

export class AuthService {
  static async signIn(username: string, password: string) {
    return await http.post('/auth/login', {
      username: username,
      password: password
    })
  }
  static async getProfile(jwt: string) {
    return await http.get('/auth/profile', {
      headers: {
        Authorization: `Bearer ${jwt}`
      }
    })
  }
}
